# Introduction

Localgo is an predictive algorithme useful to extract an simple solution from **scattered statistic** .
It is useful in particular to predict **the renting reliability**.

Exlanations that follow assume you have already installed [nodeJS](https://nodejs.org/en/).

## Installation and first run

Once the **localgo** folder is downloaded, go to the root of the folder and enter :

    `npm --install`
    
The script can be started using :

    `node index.js`
    
Notice that this script is specific for **nodeJS** but can be easily adaped to the browser environment using [browserify](http://browserify.org/)

If you think using that script in the browser, consider using a js library to manage the generated large amount of data like [lokiJS](http://lokijs.org/#/) 
