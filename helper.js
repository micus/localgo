var shelljs = require('shelljs');

// Returns true if the passed value is found in the
// array. Returns false if it is not.
Array.prototype.inArray = function (value) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == value) {
            return true;
        }
    }
    return false;
};

module.exports = {
    //cloning a object
    clone: function (obj) {
        return JSON.parse(JSON.stringify(obj));
    },
    //a log function wich allows to output to a file (default : `log.txt`) 
    //and to the console in same time
    log: function (str, file) {
        file = file || "log.txt";
        (str + "\n").toEnd(file);
        console.log(str);
    },
    //allow to fill an array with a given value
    fillArray: function (arr, valeur, dimension) {
        dimension = dimension || arr.length;
        for (var i = 0; i < dimension; i++) {
            arr[i] = valeur;
        }
        return arr;
    },
    //allow to get the global sum of an array **arr1** (if **arr2** is not present)
    //or to get the sum of elements in **arr1** which have their indexes present in **arr2** 
    sumArray: function (arr1, arr2) {
        var sum = 0;
        if (arr2) {
            for (var i = 0; i < arr2.length; i++) {
                sum += arr1[arr2[i]];
            }
        } else {
            for (var i = 0; i < arr1.length; i++) {
                sum += arr1[i];
            }
        }
        return sum;
    },
    //test if **object1** conforms to **object2** properties, if it is the case it return `true`
    //else it return `false` . The **ignore** array is *optional* and correspond to properties 
    //in **objet2** that should be excluded for the comparaison. 
    fulfill: function (objet1, objet2, ignore) {
        ignore = ignore || [];
        var answer = true;
        for (var key in objet2) {
            if (!ignore.inArray(key)) {
                if (objet1[key]) {
                    if (objet1[key] != objet2[key]) {
                        answer = false;
                    }
                } else {
                    answer = false;
                }
            }
        }
        return answer;
    },
//this function takes un objet and return an array of index 
//from a global array where the object condition is satisfied
extract : function(objet, arr, ignore) {
    var newarr = [];
    for (var i = 0; i < arr.length; i++) {
        if (this.fulfill(arr[i], objet, ignore)) {
            newarr.push(i);
        }
    }
    
    return newarr ;
}
};