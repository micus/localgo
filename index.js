// LOCALGO 0.0.1
// ----------------
// That script allows the quick calculation of the percentage arrear for a given subset of the population.
// It is based on few simplistic assumptions.
// 1. In a given set of the population, the percentage is shared homogeneously by each element of that one.
// 2. The renormalization of the percentage for each population does not strongly affect other sets of populations  


//external library that are used for that script
var merge = require('merge');
// *shelljs* is used to log the result in a file 
var shelljs = require('shelljs');
// useful helper function found in the file `helper.js`
var helper = require('./helper') ;
// the file `statistic.js` contains all statistics used in this very script
var statistic = require('./statistic') ;


//we first erase the file `log.txt` if it's exist
shelljs.rm("log.txt");

//the variable `statistiques` contains all the statistic
var statistiques = [];
// `globalObject` contains all the generated population 
var globalObjet = [];

// putting all the statistic inside `statistiques`
statistiques.push(statistic.ressources);
statistiques.push(statistic.ageSituation);
statistiques.push(statistic.ville);
statistiques.push(statistic.origine);

// recursive function which loop throught the `statistique` variable and extract all possible combinations
// and store these later in the `globalObject` variable. 
var loopOver = function (tableau, numero, length, objet) {
    var arr = tableau[numero];
    var objetInitial = objet || {};
    for (var i = 0; i < arr.length; i++) {
        if (numero < (length - 1)) {
            loopOver(tableau, (numero + 1), length, merge(objetInitial, arr[i]));
        } else {
            var obj2 = merge(objetInitial,arr[i]) ;
            globalObjet.push(helper.clone(obj2));
        }
    }
} ;

// we call the recursive function and populate `globalObject`
loopOver(statistiques, 0, statistiques.length, {});




// for better performence, we are going to use a pure array ( `populationReference` ) instead of the `globalObject` object,
// in this array each subset of the population is define by its index where each element contain the percentage for each subset of the 
// population.
var populationReference = [] ;
//the Array `statistiqueReference` is an array of objects.
//each obect is define as follow `{ population : [an array containing indexes of a given subset of the population], pourcentage : [the corresponding pourcentage] }`
var statistiqueReference = [] ;

//The array `populationReference` is set to zero for each of its elements
helper.fillArray(populationReference,0,globalObjet.length) ;

//The construction of the array `statistiqueReference`
for(var i = 0 ; i < statistiques.length ; i++) {
 for(var j = 0 ; j < statistiques[i].length ; j++) {
     statistiqueReference.push({ "pourcentage" : statistiques[i][j].pourcentage , 
                                 "population" :  helper.clone(helper.extract(statistiques[i][j],globalObjet,["pourcentage"])) }) ;    
 }
}
//
//
// ALGORITHME 
// ------------
// the pourcentage is distributed throught each subset of the population homogeneously to conform 
// to the first assumption (see above)
for(var i=0 ; i < statistiqueReference.length ; i++) {
var nombre = statistiqueReference[i].population.length ;
var pourcentage = statistiqueReference[i].pourcentage ;
for(var j=0 ; j < nombre ; j++) {
    populationReference[statistiqueReference[i].population[j]] += pourcentage/nombre ;   
}   
}

//the pourcentage for each set is renormalized to conform to the second assumption (see above)
for(var i=0 ; i < statistiqueReference.length ; i++) {
    var sum = helper.sumArray(populationReference,statistiqueReference[i].population) ;
    var coef = statistiqueReference[i].pourcentage/sum ;
    for(var j=0 ; j < statistiqueReference[i].population.length ; j++) {
    populationReference[statistiqueReference[i].population[j]] *= coef  ;   
    }
}

//we populate the `globalObject` with the generated percentage
for(var i=0 ; i < globalObjet.length ; i++) {
    globalObjet[i].pourcentage = populationReference[i] ;    
}

JSON.stringify(globalObjet,"","\t").to("log.txt")  ;
//this is a test to check the *a posteriori* accuracy of that model
/*
for(var i=0 ; i < statistiqueReference.length ; i++) {
    console.log(helper.sumArray(populationReference,statistiqueReference[i].population)) ;  
    }
    */






