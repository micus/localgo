// Statistic file 
//-------------------------------------------
//In that file, one put statistics regarding 
//a given set of population and the corresponding
//percentage of  rental arrears.
module.exports = {
    ressources: [
        {
            salaire: "+2000",
            pourcentage: 11
    },
        {
            salaire: "1250-1500",
            pourcentage: 14
    },
        {
            salaire: "1000-1250",
            pourcentage: 16
    },
        {
            salaire: "750-1000",
            pourcentage: 18
    },
        {
            salaire: "500-750",
            pourcentage: 14
    },
        {
            salaire: "-500",
            pourcentage: 16
    }
    ],


    ageSituation: [
        {
            age: "-30",
            situation: "seule",
            pourcentage: 9.8
    },
        {
            age: "-30",
            situation: "couple-sans-enfant",
            pourcentage: 4.8
    },
        {
            age: "-30",
            situation: "couple-avec-enfant",
            pourcentage: 2.8
    },
        {
            age: "-30",
            situation: "famille-monoparentale",
            pourcentage: 2.6
    },
    //---------
        {
            age: "30-39",
            situation: "seule",
            pourcentage: 11.1
    },
        {
            age: "30-39",
            situation: "couple-sans-enfant",
            pourcentage: 3.12
    },
        {
            age: "30-39",
            situation: "couple-avec-enfant",
            pourcentage: 4.4
    },
        {
            age: "30-39",
            situation: "famille-monoparentale",
            pourcentage: 5.7
    },
    //--------
        {
            age: "40-49",
            situation: "seule",
            pourcentage: 10.8
    },
        {
            age: "40-49",
            situation: "couple-sans-enfant",
            pourcentage: 2.7
    },
        {
            age: "40-49",
            situation: "couple-avec-enfant",
            pourcentage: 8.4
    },
        {
            age: "40-49",
            situation: "famille-monoparentale",
            pourcentage: 7.8
    },
    //--------
        {
            age: "50-64",
            situation: "seule",
            pourcentage: 10
    },
        {
            age: "50-64",
            situation: "couple-sans-enfant",
            pourcentage: 3.4
    },
        {
            age: "50-64",
            situation: "couple-avec-enfant",
            pourcentage: 3.4
    },
        {
            age: "50-64",
            situation: "famille-monoparentale",
            pourcentage: 3.4
    },
    //--------
        {
            age: "+65",
            situation: "seule",
            pourcentage: 3
    },
        {
            age: "+65",
            situation: "couple-sans-enfant",
            pourcentage: 0.7
    },
        {
            age: "+65",
            situation: "couple-avec-enfant",
            pourcentage: 0.5
    },
        {
            age: "+65",
            situation: "famille-monoparentale",
            pourcentage: 0.85
    }
    ],


    ville: [
        {
            typeVille: "grande-ville",
            pourcentage: 70
    },
        {
            typeVille: "moyenne-ville",
            pourcentage: 20
    },
        {
            typeVille: "petite-ville",
            pourcentage: 10
    }
    ],


    origine: [
        {
            typeProbleme: "chomage",
            pourcentage: 39
    },
        {
            typeProbleme: "retraite",
            pourcentage: 4
    },
        {
            typeProbleme: "divorce",
            pourcentage: 10
    },
        {
            typeProbleme: "sante",
            pourcentage: 18
    },
        {
            typeProbleme: "surendettement",
            pourcentage: 11
    },
        {
            typeProbleme: "loyer-trop-cher",
            pourcentage: 6
    },
        {
            typeProbleme: "litige",
            pourcentage: 12
    }
    ]
};