module.exports = function (grunt) {

    // Je préfère définir mes imports tout en haut
    /*
  grunt.loadNpmTasks('grunt-contrib-less')
  grunt.loadNpmTasks('grunt-contrib-concat')
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-contrib-watch')
	*/

    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
    var jsSrc = ['js/dev/*.js']
    var jsDist = 'js/built.js'

    // Configuration de Grunt
    grunt.initConfig({

        // **** JSDOC **** //
        'jsdoc-ng': {
            'mysubtaskname': {
                src: ['modules/*.js', './*.js', 'README.md'],
                dest: 'docs/jsdoc/',
                template: 'jsdoc-ng'
            }
        },
        docco: {
            debug: {
                src: ['modules/*.js', './*.js', 'README.md'],
                options: {
                    output: 'docs/docco/'
                }
            }
        },
        watch: {
            options: {
                livereload: true,
            },
            scripts: {
                files: 'js/dev/*.js',
                tasks: ['compile,uglify']
            },
            styles: {
                files: 'less/*.less',
                tasks: ['less']
            }
        }
    })

    grunt.registerTask('default', ['docco'])

}